import SwiftUI

struct RootScreenView<Output: RootScreenViewOutput>: ScreenView {
    @ObservedObject var output: Output
    
    var body: some View {
        VStack(spacing: 30) {
            Text("Welcome to sample")
                .bold()
                .padding()
            
            Text("To continue you must loggin")
            Button("Log in") {
                output.dispatch(.onTapOpenAuth)
            }
        }
    }
}

struct RootView_Previews: PreviewProvider {
    static var previews: some View {
        RootScreenView(output: RootScreenPreviewMockPresenter())
    }
}

fileprivate final class RootScreenPreviewMockPresenter: RootScreenViewOutput {
    var state: RootScreenViewState = .init()
    func dispatch(_ action: RootScreenViewAction) {}
}

