import Foundation
import SwiftUI
import Swinject
import SwinjectAutoregistration

enum RootScreenModule {
    static func initialize() -> UIViewController? {
        guard let presenter = defaultContainer.resolve(
            RootScreenPresenter.self,
            argument: RootScreenViewState()
        ) else { return nil }
        return UIHostingController(rootView: RootScreenView(output: presenter))
    }
    
    static func register(for container: Container) {
        container.autoregister(
            RootScreenPresenter.self,
            argument: RootScreenViewState.self,
            initializer: RootScreenPresenter.init
        )
        container.autoregister(
            RootScreenServicesProtocol.self,
            initializer: RootScreenServices.init
        )
    }
}
