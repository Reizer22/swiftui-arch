import Foundation
import Combine

typealias RootScreenComponents = (RootScreenViewAction, RootScreenViewState)
typealias RootScreenViewOutput = RootScreenPresenterProtocol

enum RootScreenViewAction {
    case onTapOpenAuth
}

struct RootScreenViewState {}

enum RootModuleAction {
    case loginRequest
    case didUserLogout
}

protocol RootScreenPresenterProtocol: PresenterProtocol where (Action, State) == RootScreenComponents {}

protocol RootScreenModuleOutput: AnyObject {
    func onAction(_ action: RootModuleAction)
}
