import Combine

enum UserAuthStatus {
    case authorized
    case unauthorized
}

protocol RootScreenAuthService: AnyObject {
    var authStatusSubject: AnyPublisher<UserAuthStatus, Never> { get }
}

protocol RootScreenServicesProtocol {
    var state: PassthroughSubject<UserAuthStatus, Never> { get }
}

final class RootScreenServices: RootScreenServicesProtocol {
    var state = PassthroughSubject<UserAuthStatus, Never>()
    
    private weak var authService: RootScreenAuthService?
    private var cancellableBag = Set<AnyCancellable>()
    
    init(authService: RootScreenAuthService?) {
        self.authService = authService
        
        authService?.authStatusSubject
            .subscribe(state)
            .store(in: &cancellableBag)
    }
}
