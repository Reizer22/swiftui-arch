import Foundation
import Combine

final class RootScreenPresenter: RootScreenPresenterProtocol {
    @Published var state: RootScreenViewState
    
    private unowned let moduleOutput: RootScreenModuleOutput
    private var services: RootScreenServicesProtocol
    private var cancellableBag = Set<AnyCancellable>()
    
    init(
        state: RootScreenViewState,
        services: RootScreenServicesProtocol,
        moduleOutput: RootScreenModuleOutput
    ) {
        self.moduleOutput = moduleOutput
        self.services = services
        self.state = state
        bindServices()
    }
    
    func dispatch(_ action: RootScreenViewAction) {
        switch action {
        case .onTapOpenAuth:
            moduleOutput.onAction(.loginRequest)
        }
    }
}

// MARK: - Private
extension RootScreenPresenter {
    private func bindServices() {
        services.state
            .sink { [weak self] userAuthStatus in
                switch userAuthStatus {
                case .authorized:
                    break
                case .unauthorized:
                    self?.moduleOutput.onAction(.didUserLogout)
                }
            }
            .store(in: &cancellableBag)
    }
}
