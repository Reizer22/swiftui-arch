import SwiftUI
import UIKit
import Swinject
import SwinjectAutoregistration

protocol RootFlowCoordinatorProtocol: BaseFlowProtocol {}

final class RootFlowCoordinator: BaseFlowCoordinator, RootFlowCoordinatorProtocol {
    private var rootController: UIViewController?
    
    func launchViewController() -> UIViewController? {
        rootController = RootScreenModule.initialize()
        return rootController
    }
    
    static func register(for container: Container) {
        container.autoregister(
            RootFlowCoordinatorProtocol.self,
            initializer: RootFlowCoordinator.init
        )
        .implements(
            RootScreenModuleOutput.self
        )
        .implements(
            AuthFlowCoordinatorDelegate.self,
            MainFlowCoordinatorDelegate.self
        )
        .inObjectScope(.container)
    }
}

extension RootFlowCoordinator: RootScreenModuleOutput {
    func onAction(_ action: RootModuleAction) {
        switch action {
        case .loginRequest:
            openAuthFlow()
        case .didUserLogout:
            closeMainFlow()
        }
    }
}

extension RootFlowCoordinator: AuthFlowCoordinatorDelegate {
    func authFlowCallback(_ callback: AuthFlowCallback) {
        switch callback {
        case .userLoggedIn:
            closeAuthFlow { [weak self] in
                self?.openMainFlow()
            }
        case .authCanceled:
            closeAuthFlow()
        }
    }
}

extension RootFlowCoordinator: MainFlowCoordinatorDelegate {
    func mainFlowCallback(_ callback: MainFlowCallback) {}
}

// MARK: - Private
extension RootFlowCoordinator {
    private func openAuthFlow() {
        guard let coordinator = defaultContainer
            .resolve(AuthFlowCoordinatorProtocol.self),
              let vc = coordinator.launchViewController()
        else { return }
        addChildFlowCoordinator(coordinator)
        rootController?.present(vc, animated: true)
    }
    
    private func closeAuthFlow(completion: (() -> Void)? = nil) {
        rootController?.dismiss(animated: true, completion: completion)
        guard let coordinator = childCoordinators.first(where: { coordinator in
            type(of: coordinator) == AuthFlowCoordinator.self
        }) as? BaseFlowProtocol else { return }
        removeChildFlowCoordinator(coordinator)
    }
    
    private func openMainFlow() {
        guard let coordinator = defaultContainer
            .resolve(MainFlowCoordinatorProtocol.self),
              let vc = coordinator.launchViewController()
        else { return }
        addChildFlowCoordinator(coordinator)
        rootController?.present(vc, animated: true)
    }
    
    private func closeMainFlow() {
        rootController?.dismiss(animated: true)
        guard let coordinator = childCoordinators.first(where: { coordinator in
            type(of: coordinator) == MainFlowCoordinator.self
        }) as? BaseFlowProtocol else { return }
        removeChildFlowCoordinator(coordinator)
    }
}
