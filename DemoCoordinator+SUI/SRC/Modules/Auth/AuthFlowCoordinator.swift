import UIKit
import Swinject
import SwinjectAutoregistration

protocol AuthFlowCoordinatorProtocol: BaseFlowProtocol {
    var delegate: AuthFlowCoordinatorDelegate? { get }
}

protocol AuthFlowCoordinatorDelegate: AnyObject {
    func authFlowCallback(_ callback: AuthFlowCallback)
}

enum AuthFlowCallback {
    case userLoggedIn
    case authCanceled
}

final class AuthFlowCoordinator: BaseFlowCoordinator, AuthFlowCoordinatorProtocol {
    weak var delegate: AuthFlowCoordinatorDelegate?
    
    private var rootViewController: UIViewController?
    
    init(delegate: AuthFlowCoordinatorDelegate?) {
        self.delegate = delegate
    }
    
    func launchViewController() -> UIViewController? {
        rootViewController = LoginScreenModule.initialize()
        return rootViewController
    }
    
    static func register(for container: Container) {
        container.autoregister(
            AuthFlowCoordinatorProtocol.self,
            initializer: AuthFlowCoordinator.init
        )
        .implements(LoginScreenModuleOutput.self)
        .inObjectScope(.container)
    }
}

extension AuthFlowCoordinator: LoginScreenModuleOutput {
    func closeAuthScreen() {
        delegate?.authFlowCallback(.userLoggedIn)
    }
}
