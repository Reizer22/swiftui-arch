import Foundation
import SwiftUI
import Combine

protocol LoginScreenAuthService: AnyObject {
    func authWith(login: String, password: String) -> AnyPublisher<Bool, Never>
}

protocol LoginServicesProtocol {
    var state: PassthroughSubject<LoginServicesState, Never> { get }
    
    func dispatch(_ action: LoginServicesAction)
}

enum LoginServicesAction {
    case auth(login: String, password: String)
}

enum LoginServicesState {
    case authInProgress
    case authResult(Bool)
}

final class LoginServices: LoginServicesProtocol {
    var state = PassthroughSubject<LoginServicesState, Never>()
    
    private weak var authService: LoginScreenAuthService?
    private var cancellableBag = Set<AnyCancellable>()
    
    init(
        authService: LoginScreenAuthService
    ) {
        self.authService = authService
    }
    
    func dispatch(_ action: LoginServicesAction) {
        switch action {
        case let .auth(login, password):
            state.send(.authInProgress)
            authRequest(login: login, password: password)
        }
    }
}

// MARK: - Private
extension LoginServices {
    private func authRequest(login: String, password: String) {
        authService?.authWith(login: login, password: password)
            .sink { [weak self] isAuthorized in
                self?.state.send(.authResult(isAuthorized))
            }
            .store(in: &cancellableBag)
    }
}
