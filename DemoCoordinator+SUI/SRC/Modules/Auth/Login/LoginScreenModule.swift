import SwiftUI
import Swinject
import SwinjectAutoregistration

enum LoginScreenModule {
    static func initialize() -> UIViewController? {
        guard let presenter = defaultContainer.resolve(
            LoginScreenPresenter.self,
            argument: LoginViewModel()
        ) else { return nil }
        
        let controller = UIHostingController(
            rootView: LoginScreenView(
                output: presenter
            )
        )
        controller.modalTransitionStyle = .coverVertical
        controller.modalPresentationStyle = .formSheet
        controller.view.backgroundColor = .clear
        
        return controller
    }
    
    static func register(for container: Container) {
        container.autoregister(
            LoginScreenPresenter.self,
            argument: LoginViewModel.self,
            initializer: LoginScreenPresenter.init
        )
        container.autoregister(
            LoginServicesProtocol.self,
            initializer: LoginServices.init
        )
    }
}
