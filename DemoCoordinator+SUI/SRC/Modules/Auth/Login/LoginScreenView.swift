import Foundation
import SwiftUI

struct LoginScreenView<Output: LoginScreenViewOutput>: ScreenView {
    @ObservedObject var output: Output
    
    var body: some View {
        VStack(spacing: 30.0) {
            Group {
                if output.state.authState != .signed {
                    VStack {
                        TextField("Login", text: loginFieldBinding)
                        TextField("Password", text: passwordFieldBinding)
                    }
                } else {
                    Image(systemName: "checkmark.circle")
                        .resizable()
                        .scaledToFit()
                        .foregroundColor(.green)
                }
            }
            if output.state.authState != .signed {
                HStack(spacing: 10) {
                    Button {
                        output.dispatch(
                            .onTapSwap
                        )
                    } label: {
                        Text("Swap")
                            .frame(
                                maxWidth: 150,
                                maxHeight: 40
                            )
                            .background(
                                output.state.authState == .loading
                                ? Color.gray.opacity(0.3)
                                : Color.orange.opacity(0.3)
                            )
                            .cornerRadius(10)
                    }
                    .disabled(output.state.authState == .loading)
                    
                    Button {
                        output.dispatch(
                            .onTapPass
                        )
                    } label: {
                        Text("Pass")
                            .frame(
                                maxWidth: 150,
                                maxHeight: 40
                            )
                            .background(
                                output.state.authState == .loading
                                ? Color.gray.opacity(0.3)
                                : Color.blue.opacity(0.3)
                            )
                            .cornerRadius(10)
                    }
                    .disabled(output.state.authState == .loading)
                    
                    Button {
                        output.dispatch(
                            .onTapSignIn
                        )
                    } label: {
                        Group {
                            if output.state.authState == .loading,
                               #available(iOS 14.0, *) {
                                ProgressView()
                                    .progressViewStyle(.circular)
                            } else {
                                Text("Sign In")
                            }
                        }
                        .frame(
                            maxWidth: 150,
                            maxHeight: 40
                        )
                        .background(
                            buttonColor
                        )
                        .cornerRadius(10)
                    }
                }
            }
        }
        .padding(15)
        .animation(.easeInOut(duration: 0.25))
        .background(
            Rectangle()
                .fill(.white)
                .cornerRadius(15)
                .shadow(
                    color: output.state.authState == .signed
                    ? .clear
                    : .black.opacity(0.3),
                    radius: 15,
                    x: -2,
                    y: 4
                )
        )
        .padding(45)
    }
    
    private var buttonColor: Color {
        switch output.state.authState {
        case .initial:
            return Color.gray.opacity(0.3)
        case .rejected:
            return Color.red.opacity(0.3)
        case .signed:
            return Color.green.opacity(0.3)
        case .loading:
            return Color.clear.opacity(0.3)
        }
    }
    
    private var loginFieldBinding: Binding<String> {
        .init {
            output.state.login
        } set: { string in
            output.dispatch(.onUserChangeLogin(string))
        }
    }
    
    private var passwordFieldBinding: Binding<String> {
        .init {
            output.state.password
        } set: { string in
            output.dispatch(.onUserChangePassword(string))
        }
    }
}

struct LoginScreenPreviewProvider: PreviewProvider {
    static var previews: some View {
        LoginScreenView(output: LoginScreenMockPresenter())
    }
}

fileprivate final class LoginScreenMockPresenter: LoginScreenPresenterProtocol {
    var state: LoginViewModel = .init()
    func dispatch(_ action: LoginScreenViewAction) {}
}
