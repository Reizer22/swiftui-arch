import Foundation
import Combine

typealias LoginScreenComponents = (LoginScreenViewAction, LoginViewModel)
typealias LoginScreenViewOutput = LoginScreenPresenterProtocol

enum LoginScreenViewAction {
    case onTapSignIn
    case onTapSwap
    case onTapPass
    case onUserChangeLogin(String)
    case onUserChangePassword(String)
}

enum LoginScreenState {
    case initial
    case signed
    case rejected
    case loading
}

struct LoginViewModel {
    var authState: LoginScreenState = .initial
    var login: String = ""
    var password: String = ""
}

protocol LoginScreenPresenterProtocol: PresenterProtocol where (Action, State) == LoginScreenComponents {}

protocol LoginScreenModuleOutput {
    func closeAuthScreen()
}
