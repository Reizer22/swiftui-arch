import Foundation
import SwiftUI
import Combine

class LoginScreenPresenter: LoginScreenPresenterProtocol {
    @Published var state: LoginViewModel
    
    private var cancellableBag = Set<AnyCancellable>()
    private var services: LoginServicesProtocol
    private var moduleOutput: LoginScreenModuleOutput!
    
    init(
        state: LoginViewModel,
        services: LoginServicesProtocol,
        moduleOutput: LoginScreenModuleOutput?
    ) {
        self.state = state
        self.services = services
        self.moduleOutput = moduleOutput
        closeWhenSigned()
        bindServices()
    }
    
    func dispatch(_ action: LoginScreenViewAction) {
        switch action {
        case .onTapSignIn:
            services.dispatch(.auth(login: state.login, password: state.password))
        case .onTapSwap:
            swapPasswordAndLogin()
        case .onTapPass:
            passAuth()
        case let .onUserChangeLogin(newLogin):
            state.login = newLogin
        case let .onUserChangePassword(newPassword):
            state.password = newPassword
        }
    }
}

//MARK: Private
extension LoginScreenPresenter {
    private func bindServices() {
        services.state
            .sink { [weak self] state in
                switch state {
                case .authInProgress:
                    self?.state.authState = .loading
                case let .authResult(result):
                    self?.processAuth(isAuthorized: result)
                }
            }
            .store(in: &cancellableBag)
    }
    
    private func closeWhenSigned() {
        $state
            .delay(for: 0.75, scheduler: DispatchQueue.main)
            .sink { [weak self] state in
                if state.authState == .signed {
                    self?.moduleOutput?.closeAuthScreen()
                }
            }
            .store(in: &cancellableBag)
    }
    
    private func swapPasswordAndLogin() {
        let password = state.password
        state.password = state.login
        state.login = password
    }
    
    private func passAuth() {
        state.authState = .signed
    }
    
    private func processAuth(isAuthorized: Bool) {
        if isAuthorized {
            state.authState = .signed
        } else {
            state.authState = .rejected
        }

        state.login = ""
        state.password = ""
    }
}
