import UIKit
import Combine

final class SettingsScreenView: UIView {
    
    private var actionSubject = PassthroughSubject<SettingsScreenAction, Never>()
    
    @IBOutlet private weak var changeColorButton: UIButton! {
        didSet {
            changeColorButton.setTitle(
                "Change color",
                for: .normal
            )
            changeColorButton.addTarget(self, action: #selector(didTapChangeColor), for: .touchUpInside)
        }
    }
    
    @objc private func didTapChangeColor() {
        actionSubject.send(.onChangeColor)
    }
}

extension SettingsScreenView: SettingsScreenViewProtocol {
    var actionEvent: AnyPublisher<SettingsScreenAction, Never> {
        actionSubject.eraseToAnyPublisher()
    }
    
    func didStateUpdate(_ viewData: SettingsScrenViewState) {
        backgroundColor = viewData.color
    }
}
