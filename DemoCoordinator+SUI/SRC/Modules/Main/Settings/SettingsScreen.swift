import UIKit
import Combine

typealias SettingsScreenComponents = (SettingsScreenAction, SettingsScrenViewState)
typealias SettingsScreenViewOutput = SettingsScreenPresetner

struct SettingsScrenViewState {
    var color: UIColor = .white
}

enum SettingsScreenAction {
    case onChangeColor
}

protocol SettingsScreenPresetnerProtocol: PresenterProtocol where (Action, State) == SettingsScreenComponents {}

protocol SettingsScreenModuleOutput: AnyObject {}

protocol SettingsScreenViewProtocol: UIView, AssociatedUIView where ViewData == SettingsScrenViewState {
    var actionEvent: AnyPublisher<SettingsScreenAction, Never> { get }
}
