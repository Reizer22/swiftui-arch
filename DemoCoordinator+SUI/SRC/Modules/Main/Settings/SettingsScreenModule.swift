import UIKit
import Swinject
import SwinjectAutoregistration

enum SettingsScreenModule {
    static func initialize() -> UIViewController? {
        guard let presetner = defaultContainer
            .resolve(SettingsScreenPresetner.self)
        else { return nil }
        let vc = SettingsScreenViewController<SettingsScreenPresetner, SettingsScreenView>(output: presetner)
        return vc
    }
    
    static func register(for container: Container) {
        container.autoregister(
            SettingsScreenPresetner.self,
            initializer: SettingsScreenPresetner.init
        )
    }
}
