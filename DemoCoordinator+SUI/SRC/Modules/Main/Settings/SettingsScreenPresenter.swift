import Combine

final class SettingsScreenPresetner: SettingsScreenPresetnerProtocol {
    @Published var state: SettingsScrenViewState = .init()
    
    func dispatch(_ action: SettingsScreenAction) {
        switch action {
        case .onChangeColor:
            if state.color == .white {
                state.color = .black
            } else {
                state.color = .white
            }
        }
    }
}
