import Foundation
import UIKit
import Combine

final class SettingsScreenViewController<Output: SettingsScreenViewOutput, ViewType: SettingsScreenViewProtocol>: UIViewController, ViewOutput {
    var output: Output
    
    private var rootView: ViewType {
        view as! ViewType
    }
    private var cansellableBag = Set<AnyCancellable>()

    init(
        output: SettingsScreenViewOutput
    ) {
        self.output = output as! Output
        super.init(nibName: "SettingsScreenView", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bindOutput()
    }
}

// MARK: - Private
extension SettingsScreenViewController {
    private func bindOutput() {
        output.$state
            .sink { [weak self] state in
                self?.rootView.didStateUpdate(state)
            }
            .store(in: &cansellableBag)
        
        rootView.actionEvent
            .sink { [weak self] action in
                self?.output.dispatch(action)
            }
            .store(in: &cansellableBag)
    }
}
