import Combine
import Foundation

typealias MainScreenComponents = (MainScreenAction, MainScreenViewState)
typealias MainScreenViewOutput = MainScreenPresenterProtocol

struct MainScreenViewState {
    var userName: String
}

enum MainScreenAction {
    case onUserProfile
    case onSettings
    case showError
    case showLoading
}

enum MainModuleAction {
    case openUserProfile
    case openSettings
}

protocol MainScreenPresenterProtocol: PresenterProtocol where (Action, State) == MainScreenComponents {
    var errorPublisher: AnyPublisher<MainErrors, Never> { get }
    var loadingPublisher: AnyPublisher<Bool, Never> { get }
}

protocol MainScreenModuleOutput: AnyObject {
    func onMainModuleAction(_ action: MainModuleAction) 
}

enum MainErrors: LocalizedError {
    case case1
    case case2
    case error(Error)
    
    public var errorDescription: String? {
        return "Custom Main Module Error"
    }
}
