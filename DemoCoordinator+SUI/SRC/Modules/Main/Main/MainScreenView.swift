import SwiftUI
import Combine

struct MainScreenView<Output: MainScreenViewOutput>: ScreenView {
    @ObservedObject var output: Output
    
    var body: some View {
        VStack(spacing: 30) {
            Text(output.state.userName)
            Button("User Profile") {
                output.dispatch(.onUserProfile)
            }
            Button("Settings") {
                output.dispatch(.onSettings)
            }
            Button("Load Error") {
                output.dispatch(.showLoading)
            }
        }
        .onReceiveError(output.errorPublisher)
        .onReceiveLoading(output.loadingPublisher)
    }
}

struct MainScreenView_Previews: PreviewProvider {
    static var previews: some View {
        MainScreenView(
            output: MockMainScreenViewOutput()
        )
    }
}

fileprivate final class MockMainScreenViewOutput: MainScreenPresenterProtocol {
    var errorPublisher: AnyPublisher<MainErrors, Never> {
        errorSubject.eraseToAnyPublisher()
    }
    
    var loadingPublisher: AnyPublisher<Bool, Never> {
        loadingSubject.eraseToAnyPublisher()
    }
    
    
    private let errorSubject = PassthroughSubject<MainErrors, Never>()
    private let loadingSubject = PassthroughSubject<Bool, Never>()
    
    var state: MainScreenViewState = .init(userName: "MockUserName")
    func dispatch(_ action: MainScreenAction) {
        switch action {
        case .onUserProfile:
//            moduleOutput.onMainModuleAction(.openUserProfile)
            break
        case .onSettings:
//            moduleOutput.onMainModuleAction(.openSettings)
            break
        case .showError:
            errorSubject.send(.case1)
        case .showLoading:
            loadingSubject.send(true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) { [weak self] in
                self?.loadingSubject.send(false)
                self?.dispatch(.showError)
            }
        }
    }
}
