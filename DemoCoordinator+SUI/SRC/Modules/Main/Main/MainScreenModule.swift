import SwiftUI
import Swinject
import SwinjectAutoregistration

enum MainScreenModule {
    static func initialize() -> UIViewController? {
        guard let presenter = defaultContainer.resolve(MainScreenPresenter.self) else {
            return nil
        }
        let vc = UIHostingController(
            rootView: MainScreenView(output: presenter)
        )

        return vc
    }
    
    static func register(for container: Container) {
        container.autoregister(
            MainScreenPresenter.self,
            initializer: MainScreenPresenter.init
        )
    }
}
