import Foundation
import Combine

protocol MainScreenUserService: AnyObject {
    var userNameSubject: AnyPublisher<UserDTO, Never> { get }
    func getCurrentUser() -> UserDTO
}

final class MainScreenPresenter: MainScreenPresenterProtocol {
    @Published var state: MainScreenViewState
    
    var errorPublisher: AnyPublisher<MainErrors, Never> {
        errorSubject.eraseToAnyPublisher()
    }
    
    var loadingPublisher: AnyPublisher<Bool, Never> {
        loadingSubject.eraseToAnyPublisher()
    }
    
    
    private let errorSubject = PassthroughSubject<MainErrors, Never>()
    private let loadingSubject = PassthroughSubject<Bool, Never>()
    private unowned let moduleOutput: MainScreenModuleOutput
    private unowned let userService: MainScreenUserService
    private var cancellableBag = Set<AnyCancellable>()
    
    init(
        moduleOutput: MainScreenModuleOutput,
        userService: MainScreenUserService
    ) {
        self.moduleOutput = moduleOutput
        self.userService = userService
        state = .init(userName: userService.getCurrentUser().fullName)
        bindUserName()
    }
    
    func dispatch(_ action: MainScreenAction) {
        switch action {
        case .onUserProfile:
            moduleOutput.onMainModuleAction(.openUserProfile)
        case .onSettings:
            moduleOutput.onMainModuleAction(.openSettings)
        case .showError:
            errorSubject.send(.case1)
        case .showLoading:
            loadingSubject.send(true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
                self?.loadingSubject.send(false)
                self?.dispatch(.showError)
            }
        }
    }
}

// MARK: - Private
extension MainScreenPresenter {
    private func bindUserName() {
        userService.userNameSubject
            .sink { [weak self] user in
                self?.state.userName = user.fullName
            }
            .store(in: &cancellableBag)
    }
}
