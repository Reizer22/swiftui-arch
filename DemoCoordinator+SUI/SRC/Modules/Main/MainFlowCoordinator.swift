import UIKit
import Combine
import Swinject
import SwinjectAutoregistration

enum MainFlowCallback {}

protocol MainFlowCoordinatorDelegate: AnyObject {
    func mainFlowCallback(_ callback: MainFlowCallback)
}

protocol MainFlowCoordinatorProtocol: BaseFlowProtocol {
    var delegate: MainFlowCoordinatorDelegate? { get }
}

final class MainFlowCoordinator: BaseFlowCoordinator, MainFlowCoordinatorProtocol {
    weak var delegate: MainFlowCoordinatorDelegate?
    private var rootController: UIViewController?
    private var navigationController: UINavigationController?
    
    init(delegate: MainFlowCoordinatorDelegate) {
        self.delegate = delegate
    }
    
    func launchViewController() -> UIViewController? {
        guard let initialVC = MainScreenModule.initialize() else { return nil }
        rootController = initialVC
        let navigationController = UINavigationController(rootViewController: initialVC)
        navigationController.modalPresentationStyle = .fullScreen
        navigationController.modalTransitionStyle = .coverVertical
        self.navigationController = navigationController
        return navigationController
    }
    
    static func register(for container: Container) {
        container.autoregister(
            MainFlowCoordinatorProtocol.self,
            initializer: MainFlowCoordinator.init
        )
        .implements(
            MainScreenModuleOutput.self,
            UserProfileScreenModuleOutput.self
        )
        .inObjectScope(.container)
    }
}

extension MainFlowCoordinator: MainScreenModuleOutput {
    func onMainModuleAction(_ action: MainModuleAction) {
        switch action {
        case .openUserProfile:
            openUserProfileScreen()
        case .openSettings:
            openSettingsScreen()
        }
    }
}

extension MainFlowCoordinator: UserProfileScreenModuleOutput {
    
}

// MARK: - Private
extension MainFlowCoordinator {
    private func openUserProfileScreen() {
        guard let vc = UserProfileScreenModule.initialize() else { return }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    private func openSettingsScreen() {
        guard let vc = SettingsScreenModule.initialize() else { return }
        navigationController?.pushViewController(vc, animated: true)
    }
}
