import Foundation
import Combine

protocol UserProfileScreenAuthService: AnyObject {
    func logout()
}

protocol UserProfileScreenUserService: AnyObject {
    var user: AnyPublisher<UserDTO, Never> { get }
    
    func setUserName(firstName: String, lastName: String)
    func getCurrenUser() -> UserDTO
}

protocol UserProfileServicesProtocol {
    var user: AnyPublisher<UserDTO, Never> { get }
    
    func dispatch(_ action: UserProfileServiceAction)
    func currentUser() -> UserDTO
}

enum UserProfileServiceAction {
    case logout
    case setUserName(firstName: String, lastName: String)
}

final class UserProfileServices: UserProfileServicesProtocol {
    var user: AnyPublisher<UserDTO, Never> {
        userService.user
    }
    private weak var authService: UserProfileScreenAuthService?
    private unowned var userService: UserProfileScreenUserService
    
    init(
        authService: UserProfileScreenAuthService,
        userService: UserProfileScreenUserService
    ) {
        self.authService = authService
        self.userService = userService
    }
    
    func dispatch(_ action: UserProfileServiceAction) {
        switch action {
        case .logout:
            authService?.logout()
        case let .setUserName(firstName, lastName):
            userService.setUserName(firstName: firstName, lastName: lastName)
        }
    }
    
    func currentUser() -> UserDTO {
        userService.getCurrenUser()
    }
}


