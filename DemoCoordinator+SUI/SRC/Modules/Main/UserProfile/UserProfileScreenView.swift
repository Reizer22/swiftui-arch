import SwiftUI

struct UserProfileScreenView<Output: UserProfileScreenViewOutput>: ScreenView {
    @ObservedObject var output: Output
    
    var body: some View {
        VStack(spacing: 30) {
            Text(output.state.fistName + " " + output.state.lastName)
                .bold()
            Button("Logout") {
                output.dispatch(.onLogout)
            }
            TextField("First Name", text: firstNameFieldBinding)
            TextField("Last Name", text: lastNameFieldBinding)
        }
        .textFieldStyle(.roundedBorder)
    }
    
    private var firstNameFieldBinding: Binding<String> {
        .init {
            output.state.fistName
        } set: { string in
            output.dispatch(.onChangeFirstName(string))
        }
    }
    
    private var lastNameFieldBinding: Binding<String> {
        .init {
            output.state.lastName
        } set: { string in
            output.dispatch(.onChangeLastName(string))
        }
    }
}

struct UserProfileScreenView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            UserProfileScreenView(output: UserProfileMockViewOutput())
        }
    }
}

fileprivate final class UserProfileMockViewOutput: UserProfileScreenViewOutput {
    var state: UserProfileScreenViewState = .init(fistName: "MockFirstName", lastName: "MockLastName")
    func dispatch(_ action: UserProfileScreenAction) {}
}
