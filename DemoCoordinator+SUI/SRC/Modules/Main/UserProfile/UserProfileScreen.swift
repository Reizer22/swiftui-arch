import Foundation

typealias UserProfileScreenComponents = (UserProfileScreenAction, UserProfileScreenViewState)
typealias UserProfileScreenViewOutput = UserProfileScreenPresenterProtocol

struct UserProfileScreenViewState {
    var fistName: String
    var lastName: String
}

enum UserProfileScreenAction {
    case onLogout
    case onChangeFirstName(String)
    case onChangeLastName(String)
}

protocol UserProfileScreenPresenterProtocol: PresenterProtocol where (Action, State) == UserProfileScreenComponents {}

protocol UserProfileScreenModuleOutput: AnyObject {}
