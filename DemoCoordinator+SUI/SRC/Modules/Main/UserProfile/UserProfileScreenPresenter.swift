import Combine

final class UserProfileScreenPresenter: UserProfileScreenPresenterProtocol {
    @Published var state: UserProfileScreenViewState
    
    private unowned let moduleOutput: UserProfileScreenModuleOutput
    private let services: UserProfileServicesProtocol
    private var cansellableBag = Set<AnyCancellable>()
    
    init(
        moduleOutput: UserProfileScreenModuleOutput,
        services: UserProfileServicesProtocol
    ) {
        self.moduleOutput = moduleOutput
        self.services = services
        let currentUser = services.currentUser()
        state = UserProfileScreenViewState(
            fistName: currentUser.fistName,
            lastName: currentUser.lastName
        )
        bindUserName()
    }
    
    func dispatch(_ action: UserProfileScreenAction) {
        switch action {
        case .onLogout:
            services.dispatch(.logout)
        case let .onChangeFirstName(firstName):
            services.dispatch(.setUserName(firstName: firstName, lastName: state.lastName))
        case let .onChangeLastName(lastName):
            services.dispatch(.setUserName(firstName: state.fistName, lastName: lastName))
        }
    }
}

// MARK: - Private
extension UserProfileScreenPresenter {
    private func bindUserName() {
        services.user
            .sink { [weak self] user in
                self?.state.lastName = user.lastName
                self?.state.fistName = user.fistName
            }
            .store(in: &cansellableBag)
    }
}
