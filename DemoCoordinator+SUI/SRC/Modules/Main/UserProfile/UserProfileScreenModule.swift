import SwiftUI
import Swinject
import SwinjectAutoregistration

enum UserProfileScreenModule {
    static func initialize() -> UIViewController? {
        guard let presenter = defaultContainer.resolve(
            UserProfileScreenPresenter.self
        ) else { return nil }
        let vc = UIHostingController(rootView: UserProfileScreenView(output: presenter))
        vc.title = "User Profile"
        return vc
    }
    
    static func register(for container: Container) {
        container.autoregister(
            UserProfileScreenPresenter.self,
            initializer: UserProfileScreenPresenter.init
        )
        container.autoregister(
            UserProfileServicesProtocol.self,
            initializer: UserProfileServices.init
        )
    }
}
