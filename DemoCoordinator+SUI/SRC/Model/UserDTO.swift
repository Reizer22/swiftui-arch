import Foundation

struct UserDTO {
    var fistName: String
    var lastName: String
    
    var fullName: String {
        fistName + " " + lastName
    }
}
