import Foundation
import Combine
import Swinject
import SwinjectAutoregistration

protocol UserServiceProtocol {
    var currentUser: CurrentValueSubject<UserDTO, Never> { get }
}

final class UserService: UserServiceProtocol {
    var currentUser = CurrentValueSubject<UserDTO, Never>(UserDTO(fistName: "", lastName: ""))
    
    private let networkService: NetworkServiceProtocol
    
    init(networkService: NetworkServiceProtocol) {
        self.networkService = networkService
        getUser()
    }
    
    static func register(for container: Container) {
        container.autoregister(
            UserServiceProtocol.self,
            initializer: UserService.init
        )
        .implements(
            UserProfileScreenUserService.self,
            MainScreenUserService.self
        )
        .inObjectScope(.container)
    }
    
    private func getUser() {
        // Possible way get user from server, for example after authorize with trigger
        currentUser.send(UserDTO(fistName: "Arthas", lastName: "Menetil"))
    }
}
