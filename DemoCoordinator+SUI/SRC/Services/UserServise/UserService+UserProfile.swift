import Combine

extension UserService: UserProfileScreenUserService {
    var user: AnyPublisher<UserDTO, Never> {
        currentUser
            .eraseToAnyPublisher()
    }
    
    func setUserName(firstName: String, lastName: String) {
        currentUser.send(UserDTO(fistName: firstName, lastName: lastName))
    }
    
    func getCurrenUser() -> UserDTO {
        currentUser.value
    }
}
