import Combine

extension UserService: MainScreenUserService {
    var userNameSubject: AnyPublisher<UserDTO, Never> {
        currentUser.eraseToAnyPublisher()
    }
    
    func getCurrentUser() -> UserDTO {
        currentUser.value
    }
}
