import Foundation
import Swinject
import SwinjectAutoregistration

protocol NetworkServiceProtocol {
    func execute<T>() -> T?
}

final class NetworkService: NetworkServiceProtocol {
    func execute<T>() -> T? {
        return nil
    }
    
    static func register(for container: Container) {
        container.autoregister(
            NetworkServiceProtocol.self,
            initializer: NetworkService.init
        )
    }
}
