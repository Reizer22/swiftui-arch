import Foundation
import Combine

extension AuthService: LoginScreenAuthService {
    func authWith(login: String, password: String) -> AnyPublisher<Bool, Never> {
        // networkService.execute()
        Future<Bool, Never> { [weak self] promise in
            self?.token.send("someTokenString")
            promise(.success(login == "Login" && password == "Password"))
        }
        .delay(for: 1.25, scheduler: DispatchQueue.main)
        .eraseToAnyPublisher()
    }
}
