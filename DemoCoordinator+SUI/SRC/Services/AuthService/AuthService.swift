import Swinject
import SwinjectAutoregistration
import Combine

protocol AuthServiceProtocol {
    var networkService: NetworkServiceProtocol { get }
}

final class AuthService: AuthServiceProtocol {
    var networkService: NetworkServiceProtocol
    var token = CurrentValueSubject<Optional<String>, Never>(nil)
    
    init(networkService: NetworkServiceProtocol) {
        self.networkService = networkService
    }
    
    static func register(for container: Container) {
        container.autoregister(
            AuthServiceProtocol.self,
            initializer: AuthService.init
        )
        .implements(
            LoginScreenAuthService.self,
            RootScreenAuthService.self,
            UserProfileScreenAuthService.self
        )
        .inObjectScope(.container)
    }
}
