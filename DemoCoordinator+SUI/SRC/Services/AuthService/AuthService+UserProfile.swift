extension AuthService: UserProfileScreenAuthService {
    func logout() {
        token.send(nil)
    }
}
