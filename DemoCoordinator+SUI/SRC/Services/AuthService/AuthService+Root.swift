import Combine

extension AuthService: RootScreenAuthService {
    var authStatusSubject: AnyPublisher<UserAuthStatus, Never> {
        token
            .compactMap { value -> UserAuthStatus in
                if value == nil {
                    return .unauthorized
                } else {
                    return .authorized
                }
            }
            .eraseToAnyPublisher()
    }
}
