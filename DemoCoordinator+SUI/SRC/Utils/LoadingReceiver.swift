import SwiftUI
import Combine

extension View {
    public func onReceiveLoading(
        _ publisher: AnyPublisher<Bool, Never>
    ) -> some View {
        modifier(
            LoadingReceiver(loadingPublisher: publisher)
        )
    }
}

private struct LoadingReceiver: ViewModifier {
    let loadingPublisher: AnyPublisher<Bool, Never>
    @State var isLoading: Bool = false
    
    func body(content: Content) -> some View {
        ZStack {
            content
                .receiver(
                    publisher: loadingPublisher,
                    receivedContent: $isLoading
                )
            
            if #available(iOS 15.0, *), isLoading {
                Color.black.opacity(0.5)
                    .frame(maxWidth: .infinity, maxHeight: .infinity)
                    .ignoresSafeArea()
                
                ProgressView().tint(.white)
            }
        }
    }
}
