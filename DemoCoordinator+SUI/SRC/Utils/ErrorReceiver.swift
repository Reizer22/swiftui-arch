import SwiftUI
import Combine

extension View {
    public func onReceiveError<Error: LocalizedError>(
        _ publisher: AnyPublisher<Error, Never>
    ) -> some View {
        modifier(
            ErrorReceiver(errorPublisher: publisher)
        )
    }
}

private struct ErrorReceiver<Error: LocalizedError>: ViewModifier {
    let errorPublisher: AnyPublisher<Error, Never>
    @State var receivedError: Error?
    
    func body(content: Content) -> some View {
        content
            .receiver(publisher: errorPublisher, receivedContent: $receivedError)
            .alert(isPresented: isShowing) {
                Alert(title: Text("Simple error!"), message: Text(receivedError?.localizedDescription ?? ""))
            }
    }
    
    var isShowing: Binding<Bool> {
        .init {
            receivedError != nil
        } set: {
            if !$0 {
                receivedError = nil
            }
        }
    }
}

enum TestError: LocalizedError {
    case case1
    
    public var errorDescription: String? {
        return "ERROR DESCRIPTION"
    }
}
