import UIKit
import SwiftUI

@main
final class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    private let rootFlowCoordinator = defaultContainer.resolve(RootFlowCoordinatorProtocol.self)

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        makeRoot(viewController: rootFlowCoordinator!.launchViewController())
        return true
    }
}

// MARK: - Private
extension AppDelegate {
    private func makeRoot(viewController: UIViewController?, animated: Bool = true) {
        guard let viewController = viewController else { return }
        if window == nil {
            window = UIWindow(frame: UIScreen.main.bounds)
        }
        if let window = window {
            if animated {
                window.rootViewController = viewController
                window.makeKeyAndVisible()
                UIView.transition(
                    with: window,
                    duration: 0.3,
                    options: [.curveEaseInOut, .transitionCrossDissolve, .preferredFramesPerSecond60],
                    animations: {}
                )
            } else {
                window.rootViewController = viewController
                window.makeKeyAndVisible()
            }
        }
    }
}
