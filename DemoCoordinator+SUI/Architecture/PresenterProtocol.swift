import Foundation
import SwiftUI
import Combine

typealias PresenterProtocol = Dispatcher & ViewState & ObservableObject

protocol ViewState {
    associatedtype State
    var state: State { get set }
}

protocol Dispatcher: AnyObject {
    associatedtype Action
    func dispatch(_ action: Action)
}
