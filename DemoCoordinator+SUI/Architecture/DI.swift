import Swinject
import SwinjectAutoregistration

var defaultContainer: Resolver {
    container.synchronize()
}

private var container = createAppContainer()

private func createAppContainer() -> Container {
    let container = Container()
    registerFlow(for: container)
    registerModules(for: container)
    registerServices(for: container)
    return container
}

private func registerFlow(for container: Container) {
    RootFlowCoordinator.register(for: container)
    AuthFlowCoordinator.register(for: container)
    MainFlowCoordinator.register(for: container)
}

private func registerModules(for container: Container) {
    RootScreenModule.register(for: container)
    MainScreenModule.register(for: container)
    LoginScreenModule.register(for: container)
    UserProfileScreenModule.register(for: container)
    SettingsScreenModule.register(for: container)
}

private func registerServices(for container: Container) {
    NetworkService.register(for: container)
    AuthService.register(for: container)
    UserService.register(for: container)
}
