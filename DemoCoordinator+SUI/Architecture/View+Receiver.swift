import SwiftUI
import Combine

private struct ViewReceiverOptional<T>: ViewModifier {
    let publisher: AnyPublisher<T, Never>
    @Binding var receivedContent: T?
    
    func body(content: Content) -> some View {
        content
            .onReceive(publisher) { output in
                receivedContent = output
            }
    }
}

private struct ViewReceiver<T>: ViewModifier {
    let publisher: AnyPublisher<T, Never>
    @Binding var receivedContent: T
    
    func body(content: Content) -> some View {
        content
            .onReceive(publisher) { output in
                receivedContent = output
            }
    }
}

extension View {
    public func receiver<T>(
        publisher: AnyPublisher<T, Never>,
        receivedContent: Binding<T?>
    ) -> some View {
        modifier(
            ViewReceiverOptional(
                publisher: publisher,
                receivedContent: receivedContent
            )
        )
    }
    
    public func receiver<T>(
        publisher: AnyPublisher<T, Never>,
        receivedContent: Binding<T>
    ) -> some View {
        modifier(
            ViewReceiver(
                publisher: publisher,
                receivedContent: receivedContent
            )
        )
    }
}
