import Foundation
import SwiftUI

protocol ViewOutput {
    associatedtype Output: PresenterProtocol
    var output: Output { get }
}

protocol ScreenView: ViewOutput, View {}

/// For UIKit UIViewController only
protocol AssociatedUIView {
    associatedtype ViewData
    func didStateUpdate(_ viewData: ViewData)
}
