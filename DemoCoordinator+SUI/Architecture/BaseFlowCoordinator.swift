import UIKit

protocol BaseFlowProtocol {
    func launchViewController() -> UIViewController?
}

class BaseFlowCoordinator: NSObject {
    
    var childCoordinators = [BaseFlowCoordinator]()
    
    public func addChildFlowCoordinator(_ flowCoordinator: BaseFlowProtocol) {
        if let flowCoordinator = flowCoordinator as? BaseFlowCoordinator {
            childCoordinators.append(flowCoordinator)
        }
    }
    
    public func removeChildFlowCoordinator(_ flowCoordinator: BaseFlowProtocol) {
        if let flowCoordinator = flowCoordinator as? BaseFlowCoordinator,
            let index = childCoordinators.firstIndex(of: flowCoordinator) {
            childCoordinators.remove(at: index)
        }
    }
}
